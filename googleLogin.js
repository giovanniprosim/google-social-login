function handleCredentialResponse(response) {
  console.log("Encoded JWT ID token: " + response.credential);

  const data = jwt_decode(response.credential)

  console.log(data)

  fullName.textContent = data.name
  sub.textContent = data.sub
  given_name.textContent = data.given_name
  family_name.textContent = data.family_name
  email.textContent = data.email
  verifiedEmail.textContent = data.email_verified
  picture.setAttribute("src", data.picture)
}

window.onload = function () {
  google.accounts.id.initialize({
    client_id: env.CLIENT_ID,
    callback: handleCredentialResponse
  });

  google.accounts.id.renderButton(
    document.getElementById("buttonDiv"),
    { theme: "outline", size: "large" }  // customization attributes
  );

  google.accounts.id.prompt(); // also display the One Tap dialog
}