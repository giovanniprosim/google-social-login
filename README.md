## References

- "Criando Google login com HTML e JS" [video tutorial](youtube.com/watch?v=92RkvBuIcts)

- Google Login Documentation [link](https://developers.google.com/identity/gsi/web/guides/overview)

- Github repository [link](github.com/jakeliny/google-login)

## Steps

### 1- Create and Configure Google Developer Console

- Enter on [Google Developer Console](https://console.cloud.google.com/apis/dashboard?pli=1) and create a new project

- Inside the project, only in first time, click on **Credentials** > button **Configure consent screen** and make your configurations.

- After *configure consent*, click on **Credentials** > button **+ Create Credentials** > **OAuth Client ID**, make your configurations and get **Your Client ID** and **Your Client Secret**.

  obs: to test, you need to configure your localhost
![Url Javascript](readme_assets/origin_js.PNG?raw=true "Url Javascript")
![Redirect Url](readme_assets/redirect.PNG?raw=true "Redirect Url")

### 2- Create and run

- Add the **Google client library** [link](https://developers.google.com/identity/gsi/web/guides/client-library).

- Add the **JWT Decode library**
```<script src="https://unpkg.com/jwt-decode/build/jwt-decode.js"></script>```

- Create **login Google button** [link](https://developers.google.com/identity/gsi/web/guides/display-button)

obs: can you generate a diffente button code using the [Code Button Generator](https://developers.google.com/identity/gsi/web/tools/configurator)

- Create *env.js* with same configurations of *env.example.js* to integrate the projects with Google Login.

- Start a server with the same URL configured on **Configure consent screen** and try to Login!